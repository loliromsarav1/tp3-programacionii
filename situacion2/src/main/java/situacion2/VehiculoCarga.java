package situacion2;

public abstract class VehiculoCarga extends Vehiculo{

    protected double kmInicial;
    protected double kmFinal;


    public VehiculoCarga(String marca, String patente, double kmInicial, double kmFinal, boolean alquilado) {
        super(marca, patente, alquilado);
        this.kmInicial = kmInicial;
        this.kmFinal = kmFinal;
    }


    public double getKmInicial() {
        return kmInicial;
    }


    public void setKmInicial(double kmInicial) {
        this.kmInicial = kmInicial;
    }


    public double getKmFinal() {
        return kmFinal;
    }


    public void setKmFinal(double kmFinal) {
        this.kmFinal = kmFinal;
    }

    public double kilometrosTotal(){
        double totalKilometros;
        totalKilometros = getKmFinal() - getKmInicial();
        return totalKilometros;
    }

    
}
