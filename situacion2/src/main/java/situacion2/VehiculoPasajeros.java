package situacion2;

public abstract class VehiculoPasajeros extends Vehiculo {

    protected int diasAlquilado;

    public VehiculoPasajeros(String marca, String patente, boolean alquilado, int diasAlquilado) {
        super(marca, patente, alquilado);
        this.diasAlquilado = diasAlquilado;
    }

    public int getDiasAlquilado() {
        return diasAlquilado;
    }

    public void setDiasAlquilado(int diasAlquilado) {
        this.diasAlquilado = diasAlquilado;
    } 

    
}
