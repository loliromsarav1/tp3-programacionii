package situacion2;

public class Vehiculo{

    protected String marca;
    protected String patente;
    protected boolean alquilado;

    

    public Vehiculo(String marca, String patente, boolean alquilado) {
        this.marca = marca;
        this.patente = patente;
        this.alquilado = alquilado;
    }

    


    public String getMarca() {
        return marca;
    }




    public void setMarca(String marca) {
        this.marca = marca;
    }


    public String getPatente() {
        return patente;
    }


    public void setPatente(String patente) {
        this.patente = patente;
    }


    public boolean getAlquilado() {
        return alquilado;
    }


    public void setAlquilado(boolean alquilado) {
        this.alquilado = alquilado;
    }


}


