package situacion2;

public interface DatosVehiculo {
    double precioAlquiler();
    void alquilarVehiculo();
    void mostrarDatosVehiculo();

}
