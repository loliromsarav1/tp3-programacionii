package situacion2;

public class Minibus  extends VehiculoPasajeros implements DatosVehiculo{

    protected int baseAlquiler;
    


    public Minibus(String marca, String patente, int baseAlquiler, int diasAlquilado, boolean alquilado) {
        super(marca, patente, alquilado, diasAlquilado);
        this.baseAlquiler = baseAlquiler;
    }


    public int getBaseAlquiler() {
        return baseAlquiler;
    }


    public void setBaseAlquiler(int baseAlquiler) {
        this.baseAlquiler = baseAlquiler;
    }


    @Override
    public double precioAlquiler() {
        
        double total, plaza, dias;
        plaza = getDiasAlquilado() * 250;
        dias = getDiasAlquilado() * 50;
        total = getBaseAlquiler() + dias + plaza;
        return total;
    }

    @Override
    public void alquilarVehiculo() {

        if(alquilado == true)
        {
            System.out.println("El vehiculo se encuentra alquilado.");
        }
        else{
            System.out.println(" ");
            System.out.println("***COMPROBANTE ALQUILER VEHICULO***");
            System.out.println(" ");
            System.out.println("Marca: "+getMarca());
            System.out.println("Patente: "+getPatente());
            System.out.println("Cantidad de días alquilado: "+getDiasAlquilado());
            System.out.println("Precio base de alquiler: "+getBaseAlquiler());
            System.out.println("Total: "+precioAlquiler());
            alquilado=true;

        }
        
    }

    @Override
    public void mostrarDatosVehiculo()
    {
        System.out.println("-----------------------------");
        System.out.println("Tipo de vehículo: MINUBÚS");
        System.out.println("Marca: "+getMarca());
        System.out.println("Patente: "+getPatente());
        alquilarVehiculo();
        
    }
    
    
}
