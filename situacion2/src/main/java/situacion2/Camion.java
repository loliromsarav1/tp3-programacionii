package situacion2;

public class Camion extends VehiculoCarga implements DatosVehiculo{

    public Camion(String marca, String patente, double kmInicial, double kmFinal, boolean alquilado) {
        super(marca, patente, kmInicial, kmFinal, alquilado);
    }


    @Override
    public double precioAlquiler() {
        
        double total;
        if(kilometrosTotal() < 50)
            total = 300;
        else
            total = 20 * kilometrosTotal();
        total = total +200;
        return total;
    }
    
    @Override
    public void alquilarVehiculo() {

        if(alquilado == true)
        {
            System.out.println("El vehiculo se encuentra alquilado.");
        }
        else{
            System.out.println(" ");
            System.out.println("***COMPROBANTE ALQUILER VEHICULO***");
            System.out.println(" ");
            System.out.println("Marca: "+getMarca());
            System.out.println("Patente: "+getPatente());
            System.out.println("Kilometraje inicial: "+getKmInicial());
            System.out.println("Kilometraje final: "+getKmFinal());  
            System.out.println("Cantidad de kilómetros recorridos: "+kilometrosTotal());
            System.out.println("Precio de alquiler: "+precioAlquiler());
            alquilado=true;

        }
        
    }

    @Override
    public void mostrarDatosVehiculo(){
        System.out.println("--------------------------");
        System.out.println("Tipo de vehiculo: CAMIÓN");
        System.out.println("Marca: "+getMarca());
        System.out.println("Patente: "+getPatente());
        alquilarVehiculo();
    }

}
