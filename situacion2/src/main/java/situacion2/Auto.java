package situacion2;

public class Auto extends VehiculoPasajeros implements DatosVehiculo {

    

    public Auto(String marca, String patente, boolean alquilado, int diasAlquilado) {
        super(marca, patente, alquilado, diasAlquilado);
    }



    @Override
    public double precioAlquiler() {
        double total;

        total = 50*getDiasAlquilado();

        return total;

    }
    
    @Override
    public void alquilarVehiculo() {

        if(alquilado == true)
        {
            System.out.println("El vehiculo se encuentra alquilado.");
        }
        else{
            System.out.println(" ");
            System.out.println("***COMPROBANTE ALQUILER VEHICULO***");
            System.out.println(" ");
            System.out.println("Marca: "+getMarca());
            System.out.println("Patente: "+getPatente());
            System.out.println("Cantidad de días alquilado: "+getDiasAlquilado());
            System.out.println("Total: "+precioAlquiler());
            alquilado=true;

        }
        
    }
    
    @Override
    public void mostrarDatosVehiculo()
    {
        System.out.println("----------------------");
        System.out.println("Tipo de vehículo: AUTO");
        System.out.println("Marca: "+getMarca());
        System.out.println("Patente: "+getPatente());
        alquilarVehiculo();       
    }

}
