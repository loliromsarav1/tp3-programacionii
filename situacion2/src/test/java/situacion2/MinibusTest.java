package situacion2;
import static org.junit.Assert.assertEquals;


import org.junit.Test;

public class MinibusTest {

    public MinibusTest(){
    }

    @Test
    public void testAlquilarVehiculo() {
        Minibus minibus = new Minibus("Fiat", "AB1256", 250, 5, false);
        assertEquals(false, minibus.alquilado);

    }

    @Test
    public void testPrecioAlquiler() {
        Minibus minibus = new Minibus("Fiat", "AB1256", 250, 5, false);
        double total = minibus.precioAlquiler();
        assertEquals(1750,total,0.00);

    }
}
