package situacion2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CamionetaTest {
    @Test
    public void testAlquilarVehiculo() {
        Camioneta camioneta = new Camioneta("Ford", "AB1256", 50, 100, true);
        assertEquals(true, camioneta.alquilado);
    }

    @Test
    public void testPrecioAlquiler() {
        Camioneta camioneta = new Camioneta("Ford", "AB1256", 50, 100, true);
        double total; 
        total = camioneta.precioAlquiler();

        assertEquals(1000,total,0.00);
        
    }
}
