package situacion1;
import org.junit.Test;
import static org.junit.Assert.*;



public class CursoTest {
    
    @Test
    public void constructor()
    {
        Curso curso = new Curso("3D", 25);
        assertEquals("3D", curso.getNombreCurso());
        assertEquals((int)25, curso.getCodigo());
    }


    @Test
    public void resetearNotas()
    {
        Estudiante estudianteUno = new Estudiante("Lila Ibáñez", 15, 4598623, "Tinogasta");
        estudianteUno.anadirCalificacion(7);
        estudianteUno.anadirCalificacion(8);
        estudianteUno.anadirCalificacion(6);

        Estudiante estudianteDos = new Estudiante("Martín Henicke", 15, 4554786, "Capital");
        estudianteDos.anadirCalificacion(5);
        estudianteDos.anadirCalificacion(3);
        estudianteDos.anadirCalificacion(2);

        Estudiante estudianteTres = new Estudiante("Juan Pérez", 15, 4547623, "Fray Mamerto Esquiú");
        estudianteTres.anadirCalificacion(8);
        estudianteTres.anadirCalificacion(7);
        estudianteTres.anadirCalificacion(9);

        Estudiante estudianteCuatro = new Estudiante("Martina Petrucci", 15, 459445, "Valle Viejo");
        estudianteCuatro.anadirCalificacion(6);
        estudianteCuatro.anadirCalificacion(5);
        estudianteCuatro.anadirCalificacion(3);

        Curso curso = new Curso("3D",25);

        curso.agregarEstudiante(estudianteUno);
        curso.agregarEstudiante(estudianteDos);
        curso.agregarEstudiante(estudianteTres);
        curso.agregarEstudiante(estudianteCuatro);


        curso.resetearNotas();
        assertEquals((int)0,estudianteUno.obtenerPromedioEstudiante());
        assertEquals((int)0,estudianteDos.obtenerPromedioEstudiante());
        assertEquals((int)0,estudianteTres.obtenerPromedioEstudiante());
        assertEquals((int)0,estudianteCuatro.obtenerPromedioEstudiante());
    }

    @Test
    public void cantidadDeEstudiantesInscriptos()
    {
        Estudiante estudianteUno = new Estudiante("Lila Ibáñez", 15, 4598623, "Tinogasta");
        Estudiante estudianteDos = new Estudiante("Martín Henicke", 15, 4554786, "Capital");
        Estudiante estudianteTres = new Estudiante("Juan Pérez", 15, 4547623, "Fray Mamerto Esquiú");
        Estudiante estudianteCuatro = new Estudiante("Martina Petrucci", 15, 459445, "Valle Viejo");

        Curso curso = new Curso("3D",25);

        curso.agregarEstudiante(estudianteUno);
        curso.agregarEstudiante(estudianteDos);
        curso.agregarEstudiante(estudianteTres);
        curso.agregarEstudiante(estudianteCuatro);

        assertEquals((int)4, curso.cantidadDeEstudiantesInscriptos());
    }

    @Test
    public void estudiantes()
    {
        Estudiante estudianteUno = new Estudiante("Lila Ibáñez", 15, 4598623, "Tinogasta");
        Estudiante estudianteDos = new Estudiante("Martín Henicke", 15, 4554786, "Capital");
        Estudiante estudianteTres = new Estudiante("Juan Pérez", 15, 4547623, "Fray Mamerto Esquiú");
        Estudiante estudianteCuatro = new Estudiante("Martina Petrucci", 15, 459445, "Valle Viejo");

        Curso curso = new Curso("3D",25);

        curso.agregarEstudiante(estudianteUno);
        curso.agregarEstudiante(estudianteDos);
        curso.agregarEstudiante(estudianteTres);
        curso.agregarEstudiante(estudianteCuatro);

        assertEquals("Lila Ibáñez", curso.estudiantes().get(0).getNombre());
        assertEquals("Martín Henicke", curso.estudiantes().get(1).getNombre());
        assertEquals("Juan Pérez", curso.estudiantes().get(2).getNombre());
        assertEquals("Martina Petrucci", curso.estudiantes().get(3).getNombre());

    }

    @Test
    public void unDesastre()
    {
        Estudiante estudianteUno = new Estudiante("Lila Ibáñez", 15, 4598623, "Tinogasta");
        estudianteUno.anadirCalificacion(7);
        estudianteUno.anadirCalificacion(8);
        estudianteUno.anadirCalificacion(6);

        Estudiante estudianteDos = new Estudiante("Martín Henicke", 15, 4554786, "Capital");
        estudianteDos.anadirCalificacion(5);
        estudianteDos.anadirCalificacion(3);
        estudianteDos.anadirCalificacion(2);

        Estudiante estudianteTres = new Estudiante("Juan Pérez", 15, 4547623, "Fray Mamerto Esquiú");
        estudianteTres.anadirCalificacion(8);
        estudianteTres.anadirCalificacion(7);
        estudianteTres.anadirCalificacion(9);

        Estudiante estudianteCuatro = new Estudiante("Martina Petrucci", 15, 459445, "Valle Viejo");
        estudianteCuatro.anadirCalificacion(6);
        estudianteCuatro.anadirCalificacion(5);
        estudianteCuatro.anadirCalificacion(3);

        Curso curso = new Curso("3D",25);

        curso.agregarEstudiante(estudianteUno);
        curso.agregarEstudiante(estudianteDos);
        curso.agregarEstudiante(estudianteTres);
        curso.agregarEstudiante(estudianteCuatro);


        assertFalse(curso.unDesastre());
    }

    @Test
    public void promedioDeCalificaciones()
    {
        Estudiante estudianteUno = new Estudiante("Lila Ibáñez", 15, 4598623, "Tinogasta");
        estudianteUno.anadirCalificacion(7);   //7
        estudianteUno.anadirCalificacion(8);
        estudianteUno.anadirCalificacion(6);

        Estudiante estudianteDos = new Estudiante("Martín Henicke", 15, 4554786, "Capital");
        estudianteDos.anadirCalificacion(5);
        estudianteDos.anadirCalificacion(3); //3
        estudianteDos.anadirCalificacion(1);

        Estudiante estudianteTres = new Estudiante("Juan Pérez", 15, 4547623, "Fray Mamerto Esquiú");
        estudianteTres.anadirCalificacion(8);
        estudianteTres.anadirCalificacion(7); //8
        estudianteTres.anadirCalificacion(9);

        Estudiante estudianteCuatro = new Estudiante("Martina Petrucci", 15, 459445, "Valle Viejo");
        estudianteCuatro.anadirCalificacion(6);
        estudianteCuatro.anadirCalificacion(5); //6
        estudianteCuatro.anadirCalificacion(7);

        Curso curso = new Curso("3D",25);

        curso.agregarEstudiante(estudianteUno);
        curso.agregarEstudiante(estudianteDos);
        curso.agregarEstudiante(estudianteTres);
        curso.agregarEstudiante(estudianteCuatro);

        assertEquals(6, curso.promedioDeCalificaciones());
    }



}
