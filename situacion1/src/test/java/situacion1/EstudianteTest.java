package situacion1;
import org.junit.Test;
import static org.junit.Assert.*;


public class EstudianteTest {
    @Test
    public void constructor()
    {
        
        Estudiante estudiante = new Estudiante("Lila Ibáñez", 15, 4598623, "Tinogasta");
        assertEquals("Lila Ibáñez",estudiante.getNombre());
        assertEquals((int)15,estudiante.getEdad());
        assertEquals((int)4598623,estudiante.getDocumento());
        assertEquals("Tinogasta",estudiante.getCiudad());

        estudiante.anadirCalificacion(7);
        estudiante.anadirCalificacion(8);
        estudiante.anadirCalificacion(6);

        assertEquals((int)7,estudiante.obtenerPromedioEstudiante());
    }
    
}
