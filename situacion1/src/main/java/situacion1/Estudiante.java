package situacion1;

import java.util.ArrayList;

public class Estudiante {

    private String nombre;
    private int edad;
    private long documento;
    private String ciudad;
    private ArrayList<Integer> calificacion; 


    

   

    public Estudiante(String nombre, int edad, long documento, String ciudad) {
        this.nombre = nombre;
        this.edad = edad;
        this.documento = documento;
        this.ciudad = ciudad;
        calificacion = new ArrayList<Integer>();
    }
    
    //métodos setters y getters

    public String getNombre() {
        return nombre;
    }



    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    public int getEdad() {
        return edad;
    }



    public void setEdad(int edad) {
        this.edad = edad;
    }



    public long getDocumento() {
        return documento;
    }



    public void setDocumento(long documento) {
        this.documento = documento;
    }



    public String getCiudad() {
        return ciudad;
    }



    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }



    public ArrayList<Integer> getCalificacion() {
        return calificacion;
    }



    public void setCalificacion(ArrayList<Integer> calificacion) {
        this.calificacion = calificacion;
    }



    //método para agregar más calificaciones a un estudiante
    public void anadirCalificacion(int calificacion)
    {
        this.calificacion.add(calificacion);

    }

    //método para obtener el promedio de un estudiante
    public int obtenerPromedioEstudiante()
    {
        int promedio = 0;

        for(int i : this.calificacion)
        {
            promedio += i;
        }
        promedio = promedio/this.calificacion.size();

        return promedio;
    }



    

    
}
