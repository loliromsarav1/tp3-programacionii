package situacion1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Curso {
    
    private String nombreCurso;
    private int codigo;
    private ArrayList<Estudiante> estudiantes;


    //método constructor

    public Curso(String nombreCurso, int codigo) {
        this.nombreCurso = nombreCurso;
        this.codigo = codigo;
        estudiantes = new ArrayList<Estudiante>();
    }

    //métodos setters y getters

    public String getNombreCurso() {
        return nombreCurso;
    }


    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }


    public int getCodigo() {
        return codigo;
    }


    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    
    /*#resetearNotas
    "Pone en cero las calificaciones de todos los estudiantes. "*/

    public boolean resetearNotas()
    {
        for(Estudiante estudiante : estudiantes)
        {
            estudiante.getCalificacion().clear();
            estudiante.getCalificacion().add(0);
        }
        return true;
    }

    /*#agregarEstudiante: unEstudiante
    "Agrega unEstudiante al curso"*/

    public void agregarEstudiante(Estudiante estudiante)
    {
        this.estudiantes.add(estudiante);
    }


    /*#cantidadDeEstudiantesInscriptos
    "Retorna la cantidad de alumnos que se inscribieron al curso"*/

    public int cantidadDeEstudiantesInscriptos()
    {
        return this.estudiantes.size();
    }

    /*#estudiantes
    "Retorna la colección de estudiantes del curso"*/

    public ArrayList<Estudiante> estudiantes()
    {
        return this.estudiantes;
    }

    /*#estudiantesAprobados
    "Retorna una colección con todos los estudiantes que aprobaron el curso (calificación
    superior o igual a 4)"*/

    public ArrayList<Estudiante> estudiantesAprobados()
    {
        ArrayList<Estudiante> aprobados = new ArrayList<Estudiante>();

        for(Estudiante i : this.estudiantes)
        {
            if(i.obtenerPromedioEstudiante() >= 4)
            {
                aprobados.add(i);
            }
        }
        return aprobados;
    }

    /*#existeEstudiante: unEstudiante
    "Indica si unEstudiante se encuentra inscripto en el curso"*/

    public boolean existeEstudiante()
    {
        if(estudiantes.isEmpty())
        {
            return false;
        }
        else{
            return true;
        }

    }

    /*#existeEstudianteConNotaDiez
    "Determina si algún alumno obtuvo la calificación 10"*/


    public boolean existeEstudianteConNotaDiez()
    {
        boolean existe = false;

        for (Estudiante estudiante : this.estudiantes)
        {
            if(estudiante.obtenerPromedioEstudiante() == 10)
            {
                existe = true;
            }
        }
        return existe;
    }

    /*#existeEstudianteLlamado: aString
    "Indica si el estudiante llamado aString se encuentra inscripto en el curso"*/

    public boolean existeEstudianteLlamado(String nombre)
    {
        boolean existe = false;

        for(Estudiante estudiante : this.estudiantes)
        {
            if(nombre == estudiante.getNombre())
            {
                existe = true;
            }
        }
        return existe;
    }

    /*#porcentajeDeAprobados
    "Retorna en porcentaje de estudiantes aprobados"*/


    public int porcentajeDeAprobados()
    {
        int porcentajeAprobados=0; 

        for(Estudiante estudiante : this.estudiantes)
        {
            if(estudiante.obtenerPromedioEstudiante() >= 4)
            {
                porcentajeAprobados += 1;
            }
        }
        porcentajeAprobados = (porcentajeAprobados*100)/estudiantes.size();
        
        return porcentajeAprobados;
    }


    /*#promedioDeCalificaciones
    "Calcula el promedio de las calificaciones obtenidas por los alumnos"*/


    public int promedioDeCalificaciones()
    {
        int promedioEstudiantes=0;

        for(Estudiante estudiante : this.estudiantes)
        {
            promedioEstudiantes += estudiante.obtenerPromedioEstudiante();
        }
        promedioEstudiantes = promedioEstudiantes/estudiantes.size();
        
        return promedioEstudiantes;
    }


    /*#estudiantes Del Interior Provinial
    "Retorna una colección con todos los estudiantes que no nacieron en la capital "*/


    public ArrayList<Estudiante> estudiantesDelInteriorProvincial()
    {
        ArrayList<Estudiante> estudiantesInterior = new ArrayList<Estudiante>();

        for(Estudiante estudiante : this.estudiantes)
        {
            if(estudiante.getCiudad() != "Capital")
            {
                estudiantesInterior.add(estudiante);
            }
        }
        return estudiantesInterior;
    }

    /*#ciudadesExceptoCapital
    "Retorna una colección, sin repeticiones, conteniendo los nombres de todas las
    ciudades do nde nacieron los alumnos inscriptos al curso"*/

    public ArrayList<String> ciudadesExceptoCapital()
    {
        ArrayList<String> ciudadesSinRepetir = new ArrayList<String>();

        for(Estudiante estudiante : this.estudiantes)
        {
            if(estudiante.getCiudad() != "Capital")
            {
                ciudadesSinRepetir.add(estudiante.getCiudad());
            }
        }
        Set<String> noRepetidas = new HashSet<>(ciudadesSinRepetir);
        ciudadesSinRepetir.clear();
        ciudadesSinRepetir.addAll(noRepetidas);
        
        return ciudadesSinRepetir;
    }


    /*#unDesastre
    "Retorna verdadero si todos los estudiantes desaprobaron el curso"*/  

    public boolean unDesastre()
    {
        boolean desaprobados = true;

        for(Estudiante estudiante : this.estudiantes)
        {
            if(estudiante.obtenerPromedioEstudiante() < 4)
            {
                desaprobados = false;
            }
        }
        return desaprobados;
    }
    

}
